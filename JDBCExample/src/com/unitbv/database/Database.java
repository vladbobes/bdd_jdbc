package com.unitbv.database;

/**
 * @author vladbobes
 *
 */
public class Database {

	private String jdbcDriver;

	private String databaseUrl;

	private String username;

	private String password;

	/**
	 * @param jdbcDriver
	 * @param databaseUrl
	 * @param username
	 * @param password
	 */
	public Database(String jdbcDriver, String databaseUrl, String username, String password) {
		this.jdbcDriver = jdbcDriver;
		this.databaseUrl = databaseUrl;
		this.username = username;
		this.password = password;
	}

	/**
	 * @return
	 */
	public String getJdbcDriver() {
		return jdbcDriver;
	}

	/**
	 * @param jdbcDriver
	 */
	public void setJdbcDriver(String jdbcDriver) {
		this.jdbcDriver = jdbcDriver;
	}

	/**
	 * @return
	 */
	public String getDatabaseUrl() {
		return databaseUrl;
	}

	/**
	 * @param databaseUrl
	 */
	public void setDatabaseUrl(String databaseUrl) {
		this.databaseUrl = databaseUrl;
	}

	/**
	 * @return
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Database [jdbcDriver=" + jdbcDriver + ", databaseUrl=" + databaseUrl + ", username=" + username
				+ ", password=" + password + "]";
	}
}
