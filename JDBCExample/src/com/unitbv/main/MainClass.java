package com.unitbv.main;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import javax.persistence.criteria.CriteriaBuilder.Case;

import com.unitbv.database.Database;
import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Copil;
import com.unitbv.model.Educator;
import com.unitbv.model.Gradinita;
import com.unitbv.model.Grupa;
import com.unitbv.model.Parinte;
import com.unitbv.util.CopilOperations;
import com.unitbv.util.EducatorOperations;
import com.unitbv.util.GradinitaOperations;
import com.unitbv.util.GrupaOperations;
import com.unitbv.util.JoinOperations;
import com.unitbv.util.ParinteOperations;

public class MainClass {

	public static void main(String[] args) throws IOException {

		int choice = 50;
		while (choice != 0) {
			System.out.println("Pick an option.\n");
			System.out.println("0. Exit");
			System.out.println("1. CRUD Copil");
			System.out.println("2. CRUD Parinte");
			System.out.println("3. CRUD Educator");
			System.out.println("4. Afiseaza toti copiii.");
			System.out.println("5. Afiseaza toti educatorii.");
			System.out.println("6. Afiseaza toti parintii.");
			System.out.println("7. Afiseaza toate grupele.");
			System.out.println("8. Afiseaza toate gradinitele.");
			System.out.println("9. Refresh locuri ocupate grupe. Recomandat dupa stergere copil.");
			System.out.println("10. Afiseaza primii 5 copii + parintii lor din baza de date.");
			System.out.println("11. Afiseaza elevii si profesorul fiecarui elev.");
			
			System.out.println("Choice = ");
			Scanner keyboard = new Scanner(System.in);
			choice = keyboard.nextInt();
			Database database = new Database("com.mysql.jdbc.Driver",
					"jdbc:mysql://localhost:3306/Gradinita?useSSL=false", "root", "asdASD123!@#");
			DatabaseConnection databaseConnection = new DatabaseConnection(database);
			CopilOperations copilOperations = new CopilOperations(databaseConnection);
			EducatorOperations educatorOperations = new EducatorOperations(databaseConnection);
			ParinteOperations parinteOperations = new ParinteOperations(databaseConnection);
			GradinitaOperations gradinitaOperations = new GradinitaOperations(databaseConnection);
			GrupaOperations grupaOperations = new GrupaOperations(databaseConnection);
			JoinOperations joinOperations = new JoinOperations(databaseConnection);
			switch (choice) {
			case 0:
				break;
			case 1:
				int crudChoiceCopil = 10;
				while (crudChoiceCopil != 0) {
					System.out.println("0. Exit.");
					System.out.println("1. Create Copil.");
					System.out.println("2. Remove Copil. ( by id )");
					System.out.println("3. Print Copil. ( by id )");
					System.out.println("4. Update Copil. ( by id )");
					crudChoiceCopil = keyboard.nextInt();
					switch (crudChoiceCopil) {
					case 0:
						try {
							databaseConnection.getConnection().close();
						} catch (SQLException e) {
							System.err.println("Failed closing streams: " + e.getMessage());
						}
						break;
					case 1:
						String numeCopil, prenumeCopil, cnpCopil;
						String dataNasteriiCopil;
						int grupaCopil;
						Scanner keyboard2 = new Scanner(System.in);
						System.out.println("");
						System.out.println("===== Adaugare copil =====");
						System.out.println("Nume copil: ");
						numeCopil = keyboard2.nextLine();
						System.out.println("Prenume copil: ");
						prenumeCopil = keyboard2.nextLine();
						System.out.println("Data nasterii(YYYY-MM-DD): ");
						dataNasteriiCopil = keyboard2.nextLine();
						System.out.println("CNP: ");
						cnpCopil = keyboard2.nextLine();
						System.out.println("Grupa: ");
						grupaCopil = keyboard2.nextInt();

						Copil copilToAdd = new Copil(numeCopil, prenumeCopil, Date.valueOf(dataNasteriiCopil), cnpCopil,
								grupaCopil);
						copilOperations.addCopil(copilToAdd);
						grupaOperations.updateGrupaLocuriOcupate(grupaCopil);
						try {
							databaseConnection.getConnection().close();
						} catch (SQLException e) {
							System.err.println("Failed closing streams: " + e.getMessage());
						}
						break;
					case 2:
						System.out.println("Input id to be removed: ");
						int idCopil = keyboard.nextInt();
						copilOperations.removeCopil(idCopil);
						try {
							databaseConnection.getConnection().close();
						} catch (SQLException e) {
							System.err.println("Failed closing streams: " + e.getMessage());
						}
						break;
					case 3:
						System.out.println("Input id to be displayed: ");
						idCopil = keyboard.nextInt();
						Copil copil = copilOperations.printCopil(idCopil);
						System.out.println(copil.toString());
						try {
							databaseConnection.getConnection().close();
						} catch (SQLException e) {
							System.err.println("Failed closing streams: " + e.getMessage());
						}
						break;
					case 4:
						System.out.println("Input id to be updated: ");
						idCopil = keyboard.nextInt();
						Scanner keyboard3 = new Scanner(System.in);
						System.out.println("Nume nou copil: ");
						numeCopil = keyboard3.nextLine();
						System.out.println("Prenume nou copil: ");
						prenumeCopil = keyboard3.nextLine();
						System.out.println("Data nasterii noua (YYYY-MM-DD): ");
						dataNasteriiCopil = keyboard3.nextLine();
						System.out.println("CNP nou: ");
						cnpCopil = keyboard3.nextLine();
						System.out.println("Grupa noua: ");
						grupaCopil = keyboard3.nextInt();

						Copil newCopil = new Copil(numeCopil, prenumeCopil, Date.valueOf(dataNasteriiCopil), cnpCopil,
								grupaCopil);
						copilOperations.updateCopil(newCopil, idCopil);
						try {
							databaseConnection.getConnection().close();
						} catch (SQLException e) {
							System.err.println("Failed closing streams: " + e.getMessage());
						}
						break;
					}
				}
				break;
			case 2:
				int crudChoiceParinte = 10;
				while (crudChoiceParinte != 0) {
					System.out.println("0. Exit.");
					System.out.println("1. Create Parinte.");
					System.out.println("2. Remove Parinte. ( by id )");
					System.out.println("3. Print Parinte. ( by id )");
					System.out.println("4. Update Parinte. ( by id )");
					crudChoiceParinte = keyboard.nextInt();
					switch (crudChoiceParinte) {
					case 0:
						try {
							databaseConnection.getConnection().close();
						} catch (SQLException e) {
							System.err.println("Failed closing streams: " + e.getMessage());
						}
						break;
					case 1:
						int idCopil;
						String nume, numarTelefon;
						Scanner keyboard2 = new Scanner(System.in);
						System.out.println("");
						System.out.println("===== Adaugare parinte =====");
						System.out.println("Nume parinte: ");
						nume = keyboard2.nextLine();
						System.out.println("Prenume copil: ");
						numarTelefon = keyboard2.nextLine();
						System.out.println("Data nasterii(YYYY-MM-DD): ");
						idCopil = keyboard2.nextInt();

						Parinte parinteToAdd = new Parinte(nume, numarTelefon, idCopil);

						parinteOperations.addParinte(parinteToAdd);
						try {
							databaseConnection.getConnection().close();
						} catch (SQLException e) {
							System.err.println("Failed closing streams: " + e.getMessage());
						}
						break;
					case 2:
						System.out.println("Input id to be removed: ");
						int idParinte = keyboard.nextInt();
						parinteOperations.removeParinte(idParinte);
						try {
							databaseConnection.getConnection().close();
						} catch (SQLException e) {
							System.err.println("Failed closing streams: " + e.getMessage());
						}
						break;
					case 3:
						System.out.println("Input id to be displayed: ");
						idParinte = keyboard.nextInt();
						Parinte parinte = parinteOperations.printParinte(idParinte);
						System.out.println(parinte.toString());
						try {
							databaseConnection.getConnection().close();
						} catch (SQLException e) {
							System.err.println("Failed closing streams: " + e.getMessage());
						}
						break;
					case 4:
						System.out.println("Input id to be updated: ");
						idParinte = keyboard.nextInt();
						Scanner keyboard3 = new Scanner(System.in);
						System.out.println("NumePrenume nou parinte: ");
						nume = keyboard3.nextLine();
						System.out.println("NrTelefon nou parinte: ");
						numarTelefon = keyboard3.nextLine();
						System.out.println("Idcopil nou: ");
						idCopil = keyboard3.nextInt();

						Parinte newParinte = new Parinte(nume, numarTelefon, idCopil);
						parinteOperations.updateParinte(newParinte, idParinte);
						try {
							databaseConnection.getConnection().close();
						} catch (SQLException e) {
							System.err.println("Failed closing streams: " + e.getMessage());
						}
						break;
					}
				}
				break;
			case 3:
				int crudChoiceEducator = 10;
				while (crudChoiceEducator != 0) {
					System.out.println("0. Exit");
					System.out.println("1. Create Educator.");
					System.out.println("2. Remove Educator. ( by id )");
					System.out.println("3. Print Educator. ( by id )");
					System.out.println("4. Update Educator. ( by id )");
					switch (crudChoiceEducator) {
					case 0:
						try {
							databaseConnection.getConnection().close();
						} catch (SQLException e) {
							System.err.println("Failed closing streams: " + e.getMessage());
						}
						break;
					case 1:
						String dataNasterii;
						String numePrenume, numarTelefon;
						int idGrupa;
						Scanner keyboard2 = new Scanner(System.in);
						System.out.println("");
						System.out.println("===== Adaugare educator =====");
						System.out.println("Nume educator: ");
						numePrenume = keyboard2.nextLine();
						System.out.println("Prenume copil: ");
						numarTelefon = keyboard2.nextLine();
						System.out.println("Data nasterii(YYYY-MM-DD): ");
						dataNasterii = keyboard2.next();
						System.out.println("idGrupa: ");
						idGrupa = keyboard2.nextInt();

						Educator educatorToAdd = new Educator(Date.valueOf(dataNasterii), numePrenume, numarTelefon,
								idGrupa);

						educatorOperations.addEducator(educatorToAdd);
						try {
							databaseConnection.getConnection().close();
						} catch (SQLException e) {
							System.err.println("Failed closing streams: " + e.getMessage());
						}
						break;
					case 2:
						System.out.println("Input id to be removed: ");
						int idEducator = keyboard.nextInt();
						educatorOperations.removeEducator(idEducator);
						try {
							databaseConnection.getConnection().close();
						} catch (SQLException e) {
							System.err.println("Failed closing streams: " + e.getMessage());
						}
						break;
					case 3:
						System.out.println("Input id to be displayed: ");
						idEducator = keyboard.nextInt();
						Educator educator = educatorOperations.printEducator(idEducator);
						System.out.println(educator.toString());
						try {
							databaseConnection.getConnection().close();
						} catch (SQLException e) {
							System.err.println("Failed closing streams: " + e.getMessage());
						}
						break;
					case 4:
						System.out.println("Input id to be updated: ");
						idEducator = keyboard.nextInt();
						Scanner keyboard3 = new Scanner(System.in);
						System.out.println("NumePrenume nou educator: ");
						numePrenume = keyboard3.nextLine();
						System.out.println("NrTelefon nou educator: ");
						numarTelefon = keyboard3.nextLine();
						System.out.println("Idgrupa nou: ");
						idGrupa = keyboard3.nextInt();
						System.out.println("Data nasterii noua: ");
						dataNasterii = keyboard3.nextLine();

						Educator newEducator = new Educator(Date.valueOf(dataNasterii), numePrenume, numarTelefon,
								idGrupa);
						educatorOperations.updateEducator(newEducator, idEducator);
						try {
							databaseConnection.getConnection().close();
						} catch (SQLException e) {
							System.err.println("Failed closing streams: " + e.getMessage());
						}
						break;
					}
				}
				break;

			case 4:
				List<Copil> copii = copilOperations.getAllCopii();
				copilOperations.printListOfCopii(copii);
				try {
					databaseConnection.getConnection().close();
				} catch (SQLException e) {
					System.err.println("Failed closing streams: " + e.getMessage());
				}
				break;
			case 5:
				List<Educator> educatori = educatorOperations.getAllEducatori();
				educatorOperations.printListOfEducatori(educatori);
				try {
					databaseConnection.getConnection().close();
				} catch (SQLException e) {
					System.err.println("Failed closing streams: " + e.getMessage());
				}
				break;
			case 6:
				List<Parinte> parinti = parinteOperations.getAllParinti();
				parinteOperations.printListOfParinti(parinti);
				try {
					databaseConnection.getConnection().close();
				} catch (SQLException e) {
					System.err.println("Failed closing streams: " + e.getMessage());
				}
				break;
			case 7:
				List<Grupa> grupe = grupaOperations.getAllGrupe();
				grupaOperations.printListOfGrupe(grupe);
				try {
					databaseConnection.getConnection().close();
				} catch (SQLException e) {
					System.err.println("Failed closing streams: " + e.getMessage());
				}
				break;
			case 8:
				List<Gradinita> gradinite = gradinitaOperations.getAllGradinite();
				gradinitaOperations.printListOfGradinite(gradinite);
				try {
					databaseConnection.getConnection().close();
				} catch (SQLException e) {
					System.err.println("Failed closing streams: " + e.getMessage());
				}
				break;
			case 9:
				grupaOperations.updateGrupaLocuriOcupate(1);
				grupaOperations.updateGrupaLocuriOcupate(2);
				break;
			case 10:
				joinOperations.printFiveKidsWithParents();
				break;
			case 11:
				joinOperations.printKidsAndTheirTeachers();
				break;
			}
		}

	}
}
