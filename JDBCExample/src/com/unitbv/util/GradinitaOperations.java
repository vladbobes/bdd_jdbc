package com.unitbv.util;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Gradinita;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

/**
 * @author vladbobes
 *
 */
public class GradinitaOperations {

	private DatabaseConnection databaseConnection;

	/**
	 * @param databaseConnection
	 */
	public GradinitaOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * @return list of gradinite from database
	 */
	public List<Gradinita> getAllGradinite() {

		databaseConnection.createConnection();
		String query = "SELECT id_gradinita, nume_gradinita, adresa_gradinita, localitate_gradinita FROM gradinita";
		List<Gradinita> gradinite = new ArrayList<Gradinita>();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Gradinita gradinita = new Gradinita();
				gradinita.setIdGradinita(resultSet.getInt("id_gradinita"));
				gradinita.setNumeGradinita(resultSet.getString("nume_gradinita"));
				gradinita.setAdresaGradinita(resultSet.getString("adresa_gradinita"));
				gradinita.setLocalitateGradinita(resultSet.getString("localitate_gradinita"));

				gradinite.add(gradinita);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
			} catch (SQLException e) {
				System.err.println("Failed closing steams: " + e.getMessage());
			}
		}

		return gradinite;
	}

	/**
	 * @param gradinita
	 * @return true if succeeded / false if failed
	 */
	public boolean addGradinita(Gradinita gradinita) {

		databaseConnection.createConnection();
		String query = "INSTER INTO educator VALUES (?, ?, ?, ?)";
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query,
					Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next())
				gradinita.setIdGradinita(resultSet.getInt(1));

			preparedStatement.setInt(1, gradinita.getIdGradinita());
			preparedStatement.setString(2, gradinita.getNumeGradinita());
			preparedStatement.setString(3, gradinita.getLocalitateGradinita());

			resultSet.close();

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	// Update all fields of gradinita from database by ID
	/**
	 * @param gradinita
	 * @param idGradinita
	 * @return true if succeeded / false if failed
	 */
	public boolean updateGradinita(Gradinita gradinita, int idGradinita) {

		databaseConnection.createConnection();
		String query = "UPDATE gradinita SET nume_gradinita = ?, adresa_gradinita = ?, localitate_gradinita = ? WHERE id_gradinita = ?";
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			preparedStatement.setString(1, gradinita.getNumeGradinita());
			preparedStatement.setString(2, gradinita.getAdresaGradinita());
			preparedStatement.setString(3, gradinita.getLocalitateGradinita());
			preparedStatement.setInt(4, idGradinita);

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	// Read gradinita from database by ID ( printing purposes )
	/**
	 * @param idGradinita
	 * @return gradinita from database by id
	 */
	public Gradinita printGradinita(int idGradinita) {

		databaseConnection.createConnection();
		String query = "SELECT id_gradinita, nume_gradinita, adresa_gradinita, localitate_gradinita FROM gradinita WHERE id_gradinita = ?";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Gradinita gradinita = new Gradinita();
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			preparedStatement.setInt(1, idGradinita);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			gradinita.setIdGradinita(resultSet.getInt("id_gradinita"));
			gradinita.setNumeGradinita(resultSet.getString("nume_gradinita"));
			gradinita.setAdresaGradinita(resultSet.getString("adresa_gradinita"));
			gradinita.setLocalitateGradinita(resultSet.getString("localitate_gradinita"));
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return null;
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return gradinita;
	}

	// Remove gradinita from database by gradinita ID
	/**
	 * @param idGradinita
	 * @return true if succeeded / false if failed
	 */
	public boolean removeGradinita(int idGradinita) {

		databaseConnection.createConnection();
		String query = "DELETE FROM gradinita WHERE id_gradinita = ?";
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			preparedStatement.setInt(1, idGradinita);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	// Receive a list of gradinite and print all of them one by one
	/**
	 * @param gradinite
	 */
	public void printListOfGradinite(List<Gradinita> gradinite) {
		for (Gradinita gradinita : gradinite) {
			System.out.println(gradinita.toString());
		}
	}
}
