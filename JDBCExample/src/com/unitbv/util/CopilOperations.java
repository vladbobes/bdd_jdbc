package com.unitbv.util;

import com.unitbv.database.DatabaseConnection;

import com.unitbv.model.Copil;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

/**
 * @author vladbobes
 *
 */
public class CopilOperations {

	private DatabaseConnection databaseConnection;

	/**
	 * @param databaseConnection
	 */
	public CopilOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	// Read all copii from database. ( For printing purpose )
	/**
	 * @return list of copii from database
	 */
	public List<Copil> getAllCopii() {

		databaseConnection.createConnection();
		String query = "SELECT id_copil, cnp, data_nastere_copil, nume_copil, prenume_copil, id_grupa FROM copil";
		List<Copil> copii = new ArrayList<Copil>();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Copil copil = new Copil();
				copil.setIdCopil(resultSet.getInt("id_copil"));
				copil.setCnp(resultSet.getString("CNP"));
				copil.setDataNastereCopil(resultSet.getDate("data_nastere_copil"));
				copil.setNumeCopil(resultSet.getString("nume_copil"));
				copil.setPrenumeCopil(resultSet.getString("prenume_copil"));
				copil.setGrupa(resultSet.getInt("id_grupa"));

				copii.add(copil);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return copii;
	}

	// Add new copil to database.
	/**
	 * @param copil
	 * @return true if succeeded / false if failed
	 */
	public boolean addCopil(Copil copil) {

		databaseConnection.createConnection();
		String query = "INSERT INTO copil VALUES (?, ?, ?, ?, ?, ?)";
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query,
					Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next())
				copil.setIdCopil(resultSet.getInt(1));

			preparedStatement.setInt(1, copil.getIdCopil());
			preparedStatement.setString(2, copil.getNumeCopil());
			preparedStatement.setString(3, copil.getPrenumeCopil());
			preparedStatement.setDate(4, (Date) copil.getDataNastereCopil());
			preparedStatement.setString(5, copil.getCnp());
			preparedStatement.setInt(6, copil.getGrupa());

			resultSet.close();

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		try {
			databaseConnection.getConnection().close();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		}

		return true;
	}

	// Update all fields of copil from database by ID
	/**
	 * @param copil
	 * @param idCopil
	 * @return true if succeeded / false if failed
	 */
	public boolean updateCopil(Copil copil, int idCopil) {

		databaseConnection.createConnection();
		String query = "UPDATE copil SET cnp = ?, data_nastere_copil = ?, nume_copil = ?, prenume_copil = ?, id_grupa = ? WHERE id_copil = ?";
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			preparedStatement.setString(1, copil.getCnp());
			preparedStatement.setDate(2, (Date) copil.getDataNastereCopil());
			preparedStatement.setString(3, copil.getNumeCopil());
			preparedStatement.setString(4, copil.getPrenumeCopil());
			preparedStatement.setInt(5, copil.getGrupa());
			preparedStatement.setInt(6, idCopil);

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	// Read copil from database by ID ( printing purposes )
	/**
	 * @param idCopil
	 * @return copil from database by id
	 */
	public Copil printCopil(int idCopil) {

		databaseConnection.createConnection();
		String query = "SELECT id_copil, CNP, data_nastere_copil, nume_copil, prenume_copil, id_grupa FROM copil WHERE id_copil = ?";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Copil copil = new Copil();
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);

			preparedStatement.setInt(1, idCopil);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			copil.setIdCopil(resultSet.getInt("id_copil"));
			copil.setCnp(resultSet.getString("CNP"));
			copil.setDataNastereCopil(resultSet.getDate("data_nastere_copil"));
			copil.setNumeCopil(resultSet.getString("nume_copil"));
			copil.setPrenumeCopil(resultSet.getString("prenume_copil"));
			copil.setGrupa(resultSet.getInt("id_grupa"));

		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return null;
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return copil;
	}

	// Remove copil from database by copil ID
	/**
	 * @param idCopil
	 * @return true if succeeded / false if failed
	 */
	public boolean removeCopil(int idCopil) {

		databaseConnection.createConnection();
		String query = "DELETE FROM copil WHERE id_copil = ?";
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			preparedStatement.setInt(1, idCopil);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	// Receive a list of copii and print all of them one by one
	/**
	 * @param copii
	 */
	public void printListOfCopii(List<Copil> copii) {
		for (Copil copil : copii) {
			System.out.println(copil.toString());
		}
	}
}
