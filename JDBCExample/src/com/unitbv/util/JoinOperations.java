package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.unitbv.database.DatabaseConnection;

/**
 * @author vladbobes
 *
 */
public class JoinOperations {

	private DatabaseConnection databaseConnection;

	/**
	 * @param databaseConnection
	 */
	public JoinOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	// Get first 5 results for copil.id_copil = parinte.id_copil ( fields:
	// copil.id_copil, nume/prenume copil, id_parinte, numePrenume parinte,
	// parinte.id_copil
	public void printFiveKidsWithParents() {

		databaseConnection.createConnection();
		String query = "SELECT copil.id_copil, nume_copil, prenume_copil, id_parinte, "
				+ "nume_prenume_parinte, parinte.id_copil  FROM copil JOIN parinte ON "
				+ "copil.id_copil = parinte.id_copil LIMIT 5";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			System.out.println("id_copil     nume_copil     prenume_copil"
					+ "     id_parinte     nume_prenume_parinte     " + "id_copil");
			while (resultSet.next()) {
				int idCopil = resultSet.getInt("copil.id_copil");
				String numeCopil = resultSet.getString("nume_copil");
				String prenumeCopil = resultSet.getString("prenume_copil");
				String numePrenumeParinte = resultSet.getString("nume_prenume_parinte");
				int idParinte = resultSet.getInt("id_parinte");
				int idCopil2 = resultSet.getInt("parinte.id_copil");
				System.out.println("    " + idCopil + "		" + numeCopil + "		" + prenumeCopil + "		"
						+ idParinte + "   		  " + numePrenumeParinte + "  	" + idCopil2);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
	}

	// get all kids and each kid class educator
	public void printKidsAndTheirTeachers() {
		databaseConnection.createConnection();
		String query = "SELECT nume_prenume_educator, nume_copil, prenume_copil "
				+ "FROM copil JOIN educator ON copil.id_grupa = educator.id_grupa";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			System.out.println("nume_prenume_educator		nume_copil		" + "prenume_copil");
			while (resultSet.next()) {
				String numePrenumeEducator = resultSet.getString("nume_prenume_educator");
				String numeCopil = resultSet.getString("nume_copil");
				String prenumeCopil = resultSet.getString("prenume_copil");
				System.out.println(numePrenumeEducator + "   		 " + numeCopil + "   		   " + prenumeCopil);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
	}

}
