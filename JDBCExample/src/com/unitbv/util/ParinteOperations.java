package com.unitbv.util;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Parinte;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

/**
 * @author vladbobes
 *
 */
public class ParinteOperations {

	private DatabaseConnection databaseConnection;

	/**
	 * @param databaseConnection
	 */
	public ParinteOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * @return list of parinti from database
	 */
	public List<Parinte> getAllParinti() {

		databaseConnection.createConnection();
		String query = "SELECT id_parinte, nume_prenume_parinte, nr_telefon_parinte, id_copil FROM parinte";
		List<Parinte> parinti = new ArrayList<Parinte>();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Parinte parinte = new Parinte();
				parinte.setIdParinte(resultSet.getInt("id_parinte"));
				parinte.setNumePrenumeParinte(resultSet.getString("nume_prenume_parinte"));
				parinte.setNrTelefonParinte(resultSet.getString("nr_telefon_parinte"));
				parinte.setCopil(resultSet.getInt("id_copil"));

				parinti.add(parinte);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
			} catch (SQLException e) {
				System.err.println("Failed closing steams: " + e.getMessage());
			}
		}

		return parinti;
	}

	/**
	 * @param parinte
	 * @return true if succeeded / false if failed
	 */
	public boolean addParinte(Parinte parinte) {

		databaseConnection.createConnection();
		String query = "INSTER INTO grupa VALUES (?, ?, ?, ?)";
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query,
					Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next())
				parinte.setIdParinte(resultSet.getInt(1));

			preparedStatement.setInt(1, parinte.getIdParinte());
			preparedStatement.setString(2, parinte.getNumePrenumeParinte());
			preparedStatement.setString(3, parinte.getNrTelefonParinte());
			preparedStatement.setInt(4, parinte.getCopil());

			resultSet.close();

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	// Update all fields of parinte from database by ID
	/**
	 * @param parinte
	 * @param idParinte
	 * @return true if succeeded / false if failed
	 */
	public boolean updateParinte(Parinte parinte, int idParinte) {

		databaseConnection.createConnection();
		String query = "UPDATE parinte SET nume_prenume_parinte = ?, nr_telefon_parinte = ?, id_copil = ? WHERE id_parinte = ?";
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			preparedStatement.setString(1, parinte.getNumePrenumeParinte());
			preparedStatement.setString(2, parinte.getNrTelefonParinte());
			preparedStatement.setInt(3, parinte.getCopil());
			preparedStatement.setInt(4, idParinte);

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	// Read parinte from database by ID ( printing purposes )
	/**
	 * @param idParinte
	 * @return parinte from database by id
	 */
	public Parinte printParinte(int idParinte) {

		databaseConnection.createConnection();
		String query = "SELECT id_parinte, nume_prenume_parinte, nr_telefon_parinte, id_copil FROM parinte WHERE id_parinte = ?";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Parinte parinte = new Parinte();
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			preparedStatement.setInt(1, idParinte);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			parinte.setIdParinte(resultSet.getInt("id_parinte"));
			parinte.setNumePrenumeParinte(resultSet.getString("nume_prenume_parinte"));
			parinte.setNrTelefonParinte(resultSet.getString("nr_telefon_parinte"));
			parinte.setCopil(resultSet.getInt("id_copil"));

		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return null;
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return parinte;
	}

	// Remove parinte from database by parinte ID
	/**
	 * @param idParinte
	 * @return true if succeeded / false if failed
	 */
	public boolean removeParinte(int idParinte) {

		databaseConnection.createConnection();
		String query = "DELETE FROM parinte WHERE id_parinte = ?";
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			preparedStatement.setInt(1, idParinte);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	// Receive a list of parinti and print all of them one by one
	/**
	 * @param parinti
	 */
	public void printListOfParinti(List<Parinte> parinti) {
		for (Parinte parinte : parinti) {
			System.out.println(parinte.toString());
		}
	}
}
