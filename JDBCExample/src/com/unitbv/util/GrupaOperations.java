package com.unitbv.util;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Grupa;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

/**
 * @author vladbobes
 *
 */
public class GrupaOperations {

	private DatabaseConnection databaseConnection;

	/**
	 * @param databaseConnection
	 */
	public GrupaOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * @return list of grupe from database
	 */
	public List<Grupa> getAllGrupe() {

		databaseConnection.createConnection();
		String query = "SELECT id_grupa, numar_locuri, locuri_ocupate, id_gradinita FROM grupa";
		List<Grupa> grupe = new ArrayList<Grupa>();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Grupa grupa = new Grupa();
				grupa.setIdGrupa(resultSet.getInt("id_grupa"));
				grupa.setNumarLocuri(resultSet.getInt("numar_locuri"));
				grupa.setLocuriOcupate(resultSet.getInt("locuri_ocupate"));
				grupa.setIdGrupa(resultSet.getInt("id_gradinita"));

				grupe.add(grupa);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
			} catch (SQLException e) {
				System.err.println("Failed closing steams: " + e.getMessage());
			}
		}

		return grupe;
	}

	// Updates locuri_ocupate by counting copii from that class
	/**
	 * @param idGrupa
	 * @return true if succeeded / false if failed
	 */
	public boolean updateGrupaLocuriOcupate(int idGrupa) {
		databaseConnection.createConnection();
		String query = "UPDATE grupa SET locuri_ocupate = (SELECT COUNT(*) FROM copil WHERE id_grupa = ?) WHERE id_grupa = ?";
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			preparedStatement.setInt(1, idGrupa);
			preparedStatement.setInt(2, idGrupa);

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return true;
	}

	/**
	 * @param grupa
	 * @return true if succeeded / false if failed
	 */
	public boolean addGrupa(Grupa grupa) {

		databaseConnection.createConnection();
		String query = "INSERT INTO grupa VALUES (?, ?, ?, ?)";
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query,
					Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next())
				grupa.setIdGrupa(resultSet.getInt(1));

			preparedStatement.setInt(1, grupa.getIdGrupa());
			preparedStatement.setInt(2, grupa.getNumarLocuri());
			preparedStatement.setInt(3, grupa.getLocuriOcupate());
			preparedStatement.setInt(4, grupa.getGradinita());

			resultSet.close();

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	// Update all fields of grupa from database by ID
	/**
	 * @param grupa
	 * @param idGrupa
	 * @return true if succeeded / false if failed
	 */
	public boolean updateGrupa(Grupa grupa, int idGrupa) {

		databaseConnection.createConnection();
		String query = "UPDATE gradinita SET numar_locuri = ?, locuri_ocupate = ?, id_gradinita = ? WHERE id_grupa = ?";
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			preparedStatement.setInt(1, grupa.getNumarLocuri());
			preparedStatement.setInt(2, grupa.getLocuriOcupate());
			preparedStatement.setInt(3, grupa.getGradinita());
			preparedStatement.setInt(4, idGrupa);

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	// Read grupa from database by ID ( printing purposes )
	/**
	 * @param idGrupa
	 * @return grupa from database by id
	 */
	public Grupa printGrupa(int idGrupa) {

		databaseConnection.createConnection();
		String query = "SELECT id_grupa, numar_locuri, locuri_ocupate, id_gradinita FROM grupa WHERE id_grupa = ?";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Grupa grupa = new Grupa();
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			preparedStatement.setInt(1, idGrupa);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			grupa.setIdGrupa(resultSet.getInt("id_grupa"));
			grupa.setNumarLocuri(resultSet.getInt("numar_locuri"));
			grupa.setLocuriOcupate(resultSet.getInt("locuri_ocupate"));
			grupa.setGradinita(resultSet.getInt("id_gradinita"));
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return null;
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return grupa;
	}

	// Remove grupa from database by grupa ID
	/**
	 * @param idGrupa
	 * @return true if succeeded / false if failed
	 */
	public boolean removeGrupa(int idGrupa) {

		databaseConnection.createConnection();
		String query = "DELETE FROM grupa WHERE id_grupa = ?";
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			preparedStatement.setInt(1, idGrupa);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	// Receive a list of grupe and print all of them one by one
	/**
	 * @param grupe
	 */
	public void printListOfGrupe(List<Grupa> grupe) {
		for (Grupa grupa : grupe) {
			System.out.println(grupa.toString());
		}
	}
}
