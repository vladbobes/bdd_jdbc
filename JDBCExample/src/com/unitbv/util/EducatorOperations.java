package com.unitbv.util;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Educator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import java.sql.PreparedStatement;

import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

/**
 * @author vladbobes
 *
 */
public class EducatorOperations {

	private DatabaseConnection databaseConnection;

	/**
	 * @param databaseConnection
	 */
	public EducatorOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * @return list of educatori from database
	 */
	public List<Educator> getAllEducatori() {

		databaseConnection.createConnection();
		String query = "SELECT id_educator, nume_prenume_educator, data_nastere_educator, nr_telefon_educator, id_grupa FROM educator";
		List<Educator> educatori = new ArrayList<Educator>();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Educator educator = new Educator();
				educator.setIdEducator(resultSet.getInt("id_educator"));
				educator.setNumePrenumeEducator(resultSet.getString("nume_prenume_educator"));
				educator.setDataNastereEducator(resultSet.getDate("data_nastere_educator"));
				educator.setNrTelefonEducator(resultSet.getString("nr_telefon_educator"));
				educator.setGrupa(resultSet.getInt("id_grupa"));

				educatori.add(educator);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
			} catch (SQLException e) {
				System.err.println("Failed closing steams: " + e.getMessage());
			}
		}

		return educatori;
	}

	/**
	 * @param educator
	 * @return true if succeeded / false if failed
	 */
	public boolean addEducator(Educator educator) {

		databaseConnection.createConnection();
		String query = "INSTER INTO educator VALUES (?, ?, ?, ?, ?)";
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query,
					Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next())
				educator.setIdEducator(resultSet.getInt(1));

			preparedStatement.setInt(1, educator.getIdEducator());
			preparedStatement.setString(2, educator.getNumePrenumeEducator());
			preparedStatement.setDate(3, (Date) educator.getDataNastereEducator());
			preparedStatement.setString(4, educator.getNrTelefonEducator());
			preparedStatement.setInt(5, educator.getGrupa());

			resultSet.close();

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	// Update all fields of educator from database by ID
	/**
	 * @param educator
	 * @param idEducator
	 * @return true if succeeded / false if failed
	 */
	public boolean updateEducator(Educator educator, int idEducator) {

		databaseConnection.createConnection();
		String query = "UPDATE educator SET nume_prenume_educator = ?, data_nastere_educator = ?, nr_telefon_educator = ?, id_grupa = ? WHERE id_educator = ?";
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			preparedStatement.setString(1, educator.getNumePrenumeEducator());
			preparedStatement.setDate(2, (Date) educator.getDataNastereEducator());
			preparedStatement.setString(3, educator.getNrTelefonEducator());
			preparedStatement.setInt(4, educator.getGrupa());
			preparedStatement.setInt(5, idEducator);

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	// Read educator from database by ID ( printing purposes )
	/**
	 * @param idEducator
	 * @return educator from database by id
	 */
	public Educator printEducator(int idEducator) {

		databaseConnection.createConnection();
		String query = "SELECT id_educator, nume_prenume_educator, data_nastere_educator, nr_telefon_educator, id_grupa FROM educator WHERE id_educator = ?";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Educator educator = new Educator();
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			preparedStatement.setInt(1, idEducator);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			educator.setIdEducator(resultSet.getInt("id_educator"));
			educator.setNumePrenumeEducator(resultSet.getString("nume_prenume_educator"));
			educator.setDataNastereEducator(resultSet.getDate("data_nastere_educator"));
			educator.setNrTelefonEducator(resultSet.getString("nr_telefon_educator"));
			educator.setGrupa(resultSet.getInt("id_grupa"));
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return null;
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return educator;
	}

	// Remove educator from database by copil ID
	/**
	 * @param idEducator
	 * @return true if succeeded / false if failed
	 */
	public boolean removeEducator(int idEducator) {

		databaseConnection.createConnection();
		String query = "DELETE FROM educator WHERE id_educator = ?";
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = databaseConnection.getConnection().prepareStatement(query);
			preparedStatement.setInt(1, idEducator);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				preparedStatement.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	// Receive a list of educatori and print all of them one by one
	/**
	 * @param educatori
	 */
	public void printListOfEducatori(List<Educator> educatori) {
		for (Educator educator : educatori) {
			System.out.println(educator.toString());
		}
	}

}
