package com.unitbv.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the gradinita database table.
 * 
 */
@Entity
@NamedQuery(name = "Gradinita.findAll", query = "SELECT g FROM Gradinita g")
public class Gradinita implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_gradinita")
	private int idGradinita;

	@Column(name = "adresa_gradinita")
	private String adresaGradinita;

	@Column(name = "localitate_gradinita")
	private String localitateGradinita;

	@Column(name = "nume_gradinita")
	private String numeGradinita;

	public Gradinita() {
	}

	/**
	 * @param id
	 * @param adresa
	 * @param localitate
	 * @param nume
	 */
	public Gradinita(int id, String adresa, String localitate, String nume) {
		this.idGradinita = id;
		this.adresaGradinita = adresa;
		this.localitateGradinita = localitate;
		this.numeGradinita = nume;
	}

	/**
	 * @return idGradinita
	 */
	public int getIdGradinita() {
		return this.idGradinita;
	}

	/**
	 * @param idGradinita
	 */
	public void setIdGradinita(int idGradinita) {
		this.idGradinita = idGradinita;
	}

	/**
	 * @return adresaGradinita
	 */
	public String getAdresaGradinita() {
		return this.adresaGradinita;
	}

	/**
	 * @param adresaGradinita
	 */
	public void setAdresaGradinita(String adresaGradinita) {
		this.adresaGradinita = adresaGradinita;
	}

	/**
	 * @return localitateGradinita
	 */
	public String getLocalitateGradinita() {
		return this.localitateGradinita;
	}

	/**
	 * @param localitateGradinita
	 */
	public void setLocalitateGradinita(String localitateGradinita) {
		this.localitateGradinita = localitateGradinita;
	}

	/**
	 * @return numeGradinita
	 */
	public String getNumeGradinita() {
		return this.numeGradinita;
	}

	/**
	 * @param numeGradinita
	 */
	public void setNumeGradinita(String numeGradinita) {
		this.numeGradinita = numeGradinita;
	}

	@Override
	public String toString() {
		return "Gradinita [id=" + idGradinita + ", name=" + numeGradinita + ", address=" + adresaGradinita + ", city="
				+ localitateGradinita + "]";
	}
}