package com.unitbv.model;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the grupa database table.
 * 
 */
/**
 * @author vladbobes
 *
 */
@Entity
@NamedQuery(name = "Grupa.findAll", query = "SELECT g FROM Grupa g")
public class Grupa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_grupa")
	private int idGrupa;

	@Column(name = "locuri_ocupate")
	private int locuriOcupate;

	@Column(name = "numar_locuri")
	private int numarLocuri;

	@Column(name = "id_gradinita")
	private int gradinita;

	public Grupa() {
	}

	/**
	 * @param id
	 * @param locuriOcupate
	 * @param numarLocuri
	 * @param idGradinita
	 */
	public Grupa(int id, int locuriOcupate, int numarLocuri, int idGradinita) {
		this.idGrupa = id;
		this.locuriOcupate = locuriOcupate;
		this.numarLocuri = numarLocuri;
		this.gradinita = idGradinita;
	}

	/**
	 * @return idGrupa
	 */
	public int getIdGrupa() {
		return this.idGrupa;
	}

	/**
	 * @param idGrupa
	 */
	public void setIdGrupa(int idGrupa) {
		this.idGrupa = idGrupa;
	}

	/**
	 * @return locuriOcupate
	 */
	public int getLocuriOcupate() {
		return this.locuriOcupate;
	}

	/**
	 * @param locuriOcupate
	 */
	public void setLocuriOcupate(int locuriOcupate) {
		this.locuriOcupate = locuriOcupate;
	}

	/**
	 * @return numarLocuri
	 */
	public int getNumarLocuri() {
		return this.numarLocuri;
	}

	/**
	 * @param numarLocuri
	 */
	public void setNumarLocuri(int numarLocuri) {
		this.numarLocuri = numarLocuri;
	}

	/**
	 * @return gradinita
	 */
	public int getGradinita() {
		return this.gradinita;
	}

	/**
	 * @param gradinita
	 */
	public void setGradinita(int gradinita) {
		this.gradinita = gradinita;
	}

	@Override
	public String toString() {
		return "Grupa [id=" + idGrupa + ", freeSpots=" + numarLocuri + ", occupiedSpots=" + locuriOcupate
				+ ", gradinita=" + gradinita + "]";
	}

}