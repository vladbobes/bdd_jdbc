package com.unitbv.model;

import java.io.Serializable;

import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the copil database table.
 * 
 */
/**
 * @author vladbobes
 *
 */
@Entity
@NamedQuery(name = "Copil.findAll", query = "SELECT c FROM Copil c")
public class Copil implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_copil")
	private int idCopil;

	@Column(name = "CNP")
	private String cnp;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_nastere_copil")
	private Date dataNastereCopil;

	@Column(name = "nume_copil")
	private String numeCopil;

	@Column(name = "prenume_copil")
	private String prenumeCopil;

	@Column(name = "id_grupa")
	private int grupa;

	public Copil() {
	}

	/**
	 * @param nume
	 * @param prenume
	 * @param dataNasterii
	 * @param cnp
	 * @param idGrupa
	 */
	public Copil(String nume, String prenume, Date dataNasterii, String cnp, int idGrupa) {
		this.numeCopil = nume;
		this.prenumeCopil = prenume;
		this.dataNastereCopil = dataNasterii;
		this.cnp = cnp;
		this.grupa = idGrupa;
	}

	/**
	 * @return idCopil
	 */
	public int getIdCopil() {
		return this.idCopil;
	}

	/**
	 * @param idCopil
	 */
	public void setIdCopil(int idCopil) {
		this.idCopil = idCopil;
	}

	/**
	 * @return cnp
	 */
	public String getCnp() {
		return this.cnp;
	}

	/**
	 * @param cnp
	 */
	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	/**
	 * @return dataNastereCopil
	 */
	public Date getDataNastereCopil() {
		return this.dataNastereCopil;
	}

	/**
	 * @param dataNastereCopil
	 */
	public void setDataNastereCopil(Date dataNastereCopil) {
		this.dataNastereCopil = dataNastereCopil;
	}

	/**
	 * @return numeCopil
	 */
	public String getNumeCopil() {
		return this.numeCopil;
	}

	/**
	 * @param numeCopil
	 */
	public void setNumeCopil(String numeCopil) {
		this.numeCopil = numeCopil;
	}

	/**
	 * @return prenumeCopil
	 */
	public String getPrenumeCopil() {
		return this.prenumeCopil;
	}

	/**
	 * @param prenumeCopil
	 */
	public void setPrenumeCopil(String prenumeCopil) {
		this.prenumeCopil = prenumeCopil;
	}

	/**
	 * @return grupa
	 */
	public int getGrupa() {
		return this.grupa;
	}

	/**
	 * @param grupa
	 */
	public void setGrupa(int grupa) {
		this.grupa = grupa;
	}

	@Override
	public String toString() {
		return "Copil [id=" + idCopil + ", firstName=" + prenumeCopil + ", lastName=" + prenumeCopil + ", dateOfBirth="
				+ dataNastereCopil + ", classId=" + grupa + "]";
	}

}