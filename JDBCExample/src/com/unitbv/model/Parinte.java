package com.unitbv.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the parinte database table.
 * 
 */
/**
 * @author vladbobes
 *
 */
@Entity
@NamedQuery(name = "Parinte.findAll", query = "SELECT p FROM Parinte p")
public class Parinte implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_parinte")
	private int idParinte;

	@Column(name = "nr_telefon_parinte")
	private String nrTelefonParinte;

	@Column(name = "nume_prenume_parinte")
	private String numePrenumeParinte;

	@Column(name = "id_copil")
	private int copil;

	public Parinte() {
	}

	/**
	 * @param id
	 * @param nrTelefon
	 * @param numePrenume
	 * @param idCopil
	 */
	public Parinte(String nrTelefon, String numePrenume, int idCopil) {
		this.nrTelefonParinte = nrTelefon;
		this.numePrenumeParinte = numePrenume;
		this.copil = idCopil;
	}

	/**
	 * @return idParinte
	 */
	public int getIdParinte() {
		return this.idParinte;
	}

	/**
	 * @param idParinte
	 */
	public void setIdParinte(int idParinte) {
		this.idParinte = idParinte;
	}

	/**
	 * @return nrTelefonParinte
	 */
	public String getNrTelefonParinte() {
		return this.nrTelefonParinte;
	}

	/**
	 * @param nrTelefonParinte
	 */
	public void setNrTelefonParinte(String nrTelefonParinte) {
		this.nrTelefonParinte = nrTelefonParinte;
	}

	/**
	 * @return numePrenumeParinte
	 */
	public String getNumePrenumeParinte() {
		return this.numePrenumeParinte;
	}

	/**
	 * @param numePrenumeParinte
	 */
	public void setNumePrenumeParinte(String numePrenumeParinte) {
		this.numePrenumeParinte = numePrenumeParinte;
	}

	/**
	 * @param copilId
	 */
	public void setCopil(int copilId) {
		this.copil = copilId;
	}

	/**
	 * @return copilId
	 */
	public int getCopil() {
		return this.copil;
	}

	@Override
	public String toString() {
		return "Parinte [id=" + idParinte + ", fullName=" + numePrenumeParinte + ", phoneNumber=" + nrTelefonParinte
				+ ", idCopil=" + copil + "]";
	}
}