package com.unitbv.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the educator database table.
 * 
 */
/**
 * @author vladbobes
 *
 */
@Entity
@NamedQuery(name = "Educator.findAll", query = "SELECT e FROM Educator e")
public class Educator implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_educator")
	private int idEducator;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_nastere_educator")
	private Date dataNastereEducator;

	@Column(name = "nr_telefon_educator")
	private String nrTelefonEducator;

	@Column(name = "nume_prenume_educator")
	private String numePrenumeEducator;

	@Column(name = "id_grupa")
	private int grupa;

	public Educator() {
	}

	/**
	 * @param id
	 * @param dataNastere
	 * @param nrTelefon
	 * @param numePrenume
	 * @param idGrupa
	 */
	public Educator(Date dataNastere, String nrTelefon, String numePrenume, int idGrupa) {
		this.dataNastereEducator = dataNastere;
		this.nrTelefonEducator = nrTelefon;
		this.numePrenumeEducator = numePrenume;
		this.grupa = idGrupa;
	}

	/**
	 * @return idEducator
	 */
	public int getIdEducator() {
		return this.idEducator;
	}

	/**
	 * @param idEducator
	 */
	public void setIdEducator(int idEducator) {
		this.idEducator = idEducator;
	}

	/**
	 * @return dataNastereEducator
	 */
	public Date getDataNastereEducator() {
		return this.dataNastereEducator;
	}

	/**
	 * @param dataNastereEducator
	 */
	public void setDataNastereEducator(Date dataNastereEducator) {
		this.dataNastereEducator = dataNastereEducator;
	}

	/**
	 * @return nrTelefonEducator
	 */
	public String getNrTelefonEducator() {
		return this.nrTelefonEducator;
	}

	/**
	 * @param nrTelefonEducator
	 */
	public void setNrTelefonEducator(String nrTelefonEducator) {
		this.nrTelefonEducator = nrTelefonEducator;
	}

	/**
	 * @return numePrenumeEducator
	 */
	public String getNumePrenumeEducator() {
		return this.numePrenumeEducator;
	}

	/**
	 * @param numePrenumeEducator
	 */
	public void setNumePrenumeEducator(String numePrenumeEducator) {
		this.numePrenumeEducator = numePrenumeEducator;
	}

	/**
	 * @return grupa
	 */
	public int getGrupa() {
		return this.grupa;
	}

	/**
	 * @param grupa
	 */
	public void setGrupa(int grupa) {
		this.grupa = grupa;
	}

	@Override
	public String toString() {
		return "Educator [id=" + idEducator + ", fullName=" + numePrenumeEducator + ", birthDate=" + dataNastereEducator
				+ ", phoneNumber=" + nrTelefonEducator + ", classId=" + grupa + "]";
	}

}