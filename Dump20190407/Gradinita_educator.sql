-- MySQL dump 10.13  Distrib 8.0.13, for macos10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: Gradinita
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `educator`
--

DROP TABLE IF EXISTS `educator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `educator` (
  `id_educator` int(11) NOT NULL AUTO_INCREMENT,
  `nume_prenume_educator` varchar(40) NOT NULL,
  `data_nastere_educator` date NOT NULL,
  `nr_telefon_educator` varchar(15) NOT NULL,
  `id_grupa` int(11) NOT NULL,
  PRIMARY KEY (`id_educator`),
  UNIQUE KEY `nr_telefon_educator` (`nr_telefon_educator`),
  KEY `id_grupa` (`id_grupa`),
  CONSTRAINT `educator_ibfk_1` FOREIGN KEY (`id_grupa`) REFERENCES `grupa` (`id_grupa`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `educator`
--

LOCK TABLES `educator` WRITE;
/*!40000 ALTER TABLE `educator` DISABLE KEYS */;
INSERT INTO `educator` VALUES (1,'Florescu Liliana','1983-02-01','0722456345',1),(2,'Ionescu Mariana','1988-06-02','0722333345',1),(3,'Marinescu Raul','1985-03-22','0723256345',2),(4,'Lazar Vlad','1993-12-15','0724124345',2);
/*!40000 ALTER TABLE `educator` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-07 23:21:01
