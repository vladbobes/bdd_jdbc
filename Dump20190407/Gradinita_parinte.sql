-- MySQL dump 10.13  Distrib 8.0.13, for macos10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: Gradinita
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `parinte`
--

DROP TABLE IF EXISTS `parinte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `parinte` (
  `id_parinte` int(11) NOT NULL AUTO_INCREMENT,
  `nume_prenume_parinte` varchar(40) NOT NULL,
  `nr_telefon_parinte` varchar(15) NOT NULL,
  `id_copil` int(11) NOT NULL,
  PRIMARY KEY (`id_parinte`),
  UNIQUE KEY `nr_telefon_parinte` (`nr_telefon_parinte`),
  KEY `id_copil` (`id_copil`),
  CONSTRAINT `parinte_ibfk_1` FOREIGN KEY (`id_copil`) REFERENCES `copil` (`id_copil`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parinte`
--

LOCK TABLES `parinte` WRITE;
/*!40000 ALTER TABLE `parinte` DISABLE KEYS */;
INSERT INTO `parinte` VALUES (1,'Popescu Andrei','0729198423',1),(2,'Ionescu Andrei','0729198424',2),(3,'Georgesc Andrei','0729198425',3),(4,'Georgescu Andrei','0729198426',4),(5,'Basescu Andrei','0729198427',5),(6,'Bobescu Andrei','0729198428',6),(7,'Codrescu Andrei','0729198429',7),(8,'Monescu Andrei','0729198439',8),(9,'Lazarescu Laurentiu','0729148439',9),(10,'Marinescu Andreea','0729248439',10),(11,'Marin Roxana','0729243439',11),(12,'Marina Mariana','0729233439',12);
/*!40000 ALTER TABLE `parinte` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-07 23:21:01
