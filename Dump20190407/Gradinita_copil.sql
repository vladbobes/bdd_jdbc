-- MySQL dump 10.13  Distrib 8.0.13, for macos10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: Gradinita
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `copil`
--

DROP TABLE IF EXISTS `copil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `copil` (
  `id_copil` int(11) NOT NULL AUTO_INCREMENT,
  `nume_copil` varchar(20) NOT NULL,
  `prenume_copil` varchar(20) NOT NULL,
  `data_nastere_copil` date NOT NULL,
  `CNP` varchar(13) NOT NULL,
  `id_grupa` int(11) NOT NULL,
  PRIMARY KEY (`id_copil`),
  UNIQUE KEY `CNP` (`CNP`),
  KEY `id_grupa` (`id_grupa`),
  CONSTRAINT `copil_ibfk_1` FOREIGN KEY (`id_grupa`) REFERENCES `grupa` (`id_grupa`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `copil`
--

LOCK TABLES `copil` WRITE;
/*!40000 ALTER TABLE `copil` DISABLE KEYS */;
INSERT INTO `copil` VALUES (1,'Popescu','Ion','2002-11-02','1021102080060',1),(2,'Ionescu','Vasile','2003-01-01','1030101080060',1),(3,'Ionescu','George','2002-08-11','1020811080060',1),(4,'Popescu','Vasile','2003-11-02','1031102080060',1),(5,'Ionescu','Vasile','2004-01-01','1040101080060',1),(6,'Georgescu','George','2002-06-11','1020611080060',1),(7,'Andrei','Vasile','2003-08-02','1030802080060',2),(8,'Ion','Vasile','2004-02-01','1040201080060',2),(9,'Brebenaru','George','2001-06-11','1010611080060',2),(10,'Bara','Vlad','2003-09-02','1030902080060',2),(11,'Cojanu','Ionut','2003-12-01','1031201080060',2),(12,'Lucaci','Razvan','2001-12-11','1011211080060',2),(16,'Vermond','Georgica','2004-12-12','1041212080060',1);
/*!40000 ALTER TABLE `copil` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-07 23:21:01
